import * as React from 'react';



import { AgGridReact } from 'ag-grid-react';
import SampleRowDataFactory from './SampleRowData';
import ColumnDefinitionFactory from './ColumnDefinitions';
//import "ag-grid-enterprise";


import '../../../node_modules/ag-grid/dist/styles/ag-grid.css';
import '../../../node_modules/ag-grid/dist/styles/theme-fresh.css';


import * as style from './style.css';

import * as MessagesAction from '../../actions/message';

const moment = require('moment');
import { MessageList } from '../chat/MessageList'

// import FaSearch from 'react-icons/lib/fa/search';
// import FaComments from 'react-icons/lib/fa/comments';
// import FaClose from 'react-icons/lib/fa/close';
// import FaMenu from 'react-icons/lib/md/more-vert';


export namespace MainSection {
  export interface Props {
    messages: MessageItemData[];
    actions: typeof MessagesAction;
  }

  export interface State {
    columnDefs: any,
    rowData: any
  }
}

export class MainSection extends React.Component<MainSection.Props, MainSection.State> {

  constructor(props?: MainSection.Props, context?: any) {
    super(props, context);
    this.state = {
      columnDefs: new ColumnDefinitionFactory().createColDefs(),
      // set the row data to use inside the grid
      rowData: new SampleRowDataFactory().createRowData()
    };

    
  }


  
  

  
  render() {
    const { messages, actions } = this.props;
  
    return (
      <div style={{height: 525, width: '100%'}} className="ag-fresh">
        
        
        <AgGridReact
          // binding to array properties
          columnDefs={this.state.columnDefs}
          rowData={this.props.messages}

          // no simple properties
          suppressRowClickSelection={true}
          rowSelection={"multiple"}
          enableColResize={true}
          enableSorting={true}
          enableFilter={true}
          animateRows={true}
          rowHeight={40}
        />        

      </div>
    );
  }
}
