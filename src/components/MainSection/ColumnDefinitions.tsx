
import * as React from 'react';
import { Link } from 'react-router-dom';

// ag-Grid builds columns based on the column definitions returned here
export default class ColumnDefinitionFactory {

    createColDefs() {
        return [
            // first column has the checkboxes
            {
                headerName: '#', width: 20, checkboxSelection: true,
                suppressSorting: true, suppressMenu: true, pinned: true
            },
            {
                headerName: 'edit',
                field: '_id',
                suppressSorting: true, suppressMenu: true, pinned: true,
                cellRendererFramework: ProductEditLink
            },

             {
                headerName: 'Product',

                children: [
                    { field: "name", pinned: true, editable: true },


                  
                    {
                        field: "image",
                        pinned: true,
                        // supply a React component for custom cell rendering
                        cellRendererFramework: ProductImgLink
                    },

                ]
            },


            { field: "price",  headerName: "Price " , pinned: true  },
            { field: "qty",  headerName: "QTY ", pinned: true  },
            {
                headerName: "creation date", field: "createdAt",

                filter: 'date', pinned: true,
                // simple cell formatter for formatting dates
                // valueFormatter: function (params) {
                //     return formatDate(params.value);
                // }

            }


        ];
    }
}

// utility function to format date
function formatDate(value) {
    return pad(value.getDate(), 2) + '/' +
        pad(value.getMonth() + 1, 2) + '/' +
        value.getFullYear();
}

// utility function used to pad the date formatting.
function pad(num, totalStringSize) {
    let asString = num + "";
    while (asString.length < totalStringSize) asString = "0" + asString;
    return asString;
}


export class ProductEditLink extends React.Component<any, any> {

    render() {

        let link = '/edit/' + this.props.value;
        // console.log(this.props.value)
        return (
            <Link to={link}>edit</Link>
        );
    }
}

export class ProductImgLink extends React.Component<any, any> {

    render() {

        
        // console.log(this.props.value)
        return (
           <img src={this.props.value} alt="Image tag"/>
        );
    }
}