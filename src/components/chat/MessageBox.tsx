import * as React from 'react';

import * as style from './style.css';

// import FaForward from 'react-icons/lib/fa/mail-forward';
// import FaReply from 'react-icons/lib/fa/mail-reply';

// import IoDoneAll from 'react-icons/lib/io/android-done-all';
// import MdIosTime from 'react-icons/lib/md/access-time';
// import MdCheck from 'react-icons/lib/md/check';



export namespace MessageBox {
    export interface Props {
        position: any,
        type: any,
        text: any,
        title: any,
        titleColor: any,
        onTitleClick: any,
        onForwardClick: any,
        date: any,// new Date()
        data: {},
        onClick: any,
        onOpen: any,
        onDownload: any,
        forwarded: any,
        status: any,
        dateString: any,
        notch: any,
        avatar: any,
        renderAddCmp: Function,
        copiableDate: any,
    }

    export interface State {
        /* empty */
    }
}

const moment = require('moment');

const classNames = require('classnames');

export class MessageBox extends React.Component<MessageBox.Props, MessageBox.State> {
    
    public static defaultProps: Partial<MessageBox.Props> = {
        position: 'left',
        type: 'text',
        text: '',
        title: null,
        titleColor: null,
        onTitleClick: null,
        onForwardClick: null,
        date: new Date(),
        data: {},
        onClick: null,
        onOpen: null,
        onDownload: null,
        forwarded: false,
        status: null,
        dateString: null,
        notch: true,
        avatar: null,
        renderAddCmp: null,
        copiableDate: false,
    };

    constructor(props?: MessageBox.Props, context?: any) {
        super(props, context);
        
    }

    render() {
        var positionCls = classNames('rce-mbox', { 'rce-mbox-right': this.props.position === 'right' });
        var thatAbsoluteTime = this.props.type !== 'text' && this.props.type !== 'file' && !(this.props.type === 'location' && this.props.text);


        const dateText = this.props.date && !isNaN(this.props.date) && (
            this.props.dateString ||
            moment(this.props.date).calendar()
        );

        return (
            <div
                className={classNames('rce-container-mbox')}
                onClick={this.props.onClick}>
                {
                    this.props.renderAddCmp instanceof Function &&
                    this.props.renderAddCmp()
                }
                {
                    
                        <div
                            className={classNames(
                                positionCls,
                                { 'rce-mbox--clear-padding': thatAbsoluteTime },
                                { 'rce-mbox--clear-notch': !this.props.notch }
                            )}>
                            <div className='rce-mbox-body'>
                                {
                                    this.props.forwarded === true &&
                                    <div
                                        className={classNames(
                                            'rce-mbox-forward',
                                            { 'rce-mbox-forward-right': this.props.position === 'left' },
                                            { 'rce-mbox-forward-left': this.props.position === 'right' }
                                        )}
                                        onClick={this.props.onForwardClick}>
                                        {/* <FaForward /> */}
                                    </div>
                                }

                                {
                                    (this.props.title || this.props.avatar) &&
                                    <p
                                        style={this.props.titleColor && { color: this.props.titleColor }}
                                        onClick={this.props.onTitleClick}
                                        className={classNames('rce-mbox-title', {
                                            'rce-mbox-title--clear': this.props.type === 'text',
                                        })}>
                                       
                                        {
                                            this.props.title &&
                                            <span>{this.props.title}</span>
                                        }
                                    </p>
                                }

                                {
                                    this.props.type === 'text' &&
                                    <div className="rce-mbox-text">
                                        {this.props.text}
                                    </div>
                                }
                

                               

                               

                               

                                <div
                                    className={classNames(
                                        'rce-mbox-time',
                                        { 'rce-mbox-time-block': thatAbsoluteTime },
                                        { 'non-copiable': !this.props.copiableDate },
                                    )}
                                    data-text={this.props.copiableDate ? undefined : dateText}>
                                    {
                                        this.props.copiableDate &&
                                        this.props.date &&
                                        !isNaN(this.props.date) &&
                                        (
                                            this.props.dateString ||
                                            moment(this.props.date).calendar()
                                        )
                                    }
                                      
                                  
                                    {
                                        this.props.status &&
                                        <span className='rce-mbox-status'>
                                            {
                                                this.props.status === 'waiting' &&
                                                {/* <MdIosTime /> */}
                                            }

                                            {
                                                this.props.status === 'sent' &&
                                                {/* <MdCheck /> */}
                                            }

                                            {
                                                this.props.status === 'received' &&
                                                {/* <IoDoneAll /> */}
                                            }

                                            {
                                                this.props.status === 'read' &&
                                                {/* <IoDoneAll color='#4FC3F7' /> */}
                                            }
                                        </span>
                                    }
                                </div>
                            </div>

                            
                        </div>
                }
            </div>
        );
    }
}





