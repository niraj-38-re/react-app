import * as React from 'react';

// import FaChevronDown from 'react-icons/lib/fa/chevron-down';

const classNames = require('classnames');
    
import './style.css'

import * as style from './style.css';

import { MessageBox } from './MessageBox';

export namespace MessageList {
  export interface Props {
    className?:any,
    onClick?: any,
    onTitleClick?: any,
    onForwardClick?: any,
    onDownButtonClick?: any,
    onOpen?: any,
    onDownload?: any,
    dataSource?: any,
    lockable?: any,
    toBottomHeight?: any,
    downButton?: any,
    downButtonBadge?: any,
  }

  export interface State {
    scrollBottom: any,
    downButton: any,
    /* empty */
  }
}

export class MessageList extends React.Component<MessageList.Props, MessageList.State> {
    public static defaultProps: Partial<MessageList.Props> = {
        onClick: null,
        onTitleClick: null,
        onForwardClick: null,
        onDownButtonClick: null,
        onOpen: null,
        onDownload: null,
        dataSource: [],
        lockable: false,
        toBottomHeight: 300,
        downButton: true,
        downButtonBadge: null,
    };

  constructor(props?: MessageList.Props, context?: any) {
    super(props, context);
    this.state = {
        scrollBottom: 0,
        downButton: false,
    };
  }
  /*
  checkScroll() {
    var e = this.mlistRef;
    if (!e)
        return;

    if (this.props.toBottomHeight === '100%' || this.state.scrollBottom < this.props.toBottomHeight) {
        // scroll to bottom
        e.scrollTop = e.scrollHeight;
    } else {
        if (this.props.lockable === true) {
            e.scrollTop = e.scrollHeight - e.offsetHeight - this.state.scrollBottom;
        }
    }
}

componentWillReceiveProps() {
    if (!this.mlistRef)
        return;
    this.setState({
        scrollBottom: this.getBottom(this.mlistRef),
    }, this.checkScroll.bind(this));
}

getBottom(e) {
    return e.scrollHeight - e.scrollTop - e.offsetHeight;
}

onOpen(item, i, e) {
    if (this.props.onOpen instanceof Function)
        this.props.onOpen(item, i, e);
}

onDownload(item, i, e) {
    if (this.props.onDownload instanceof Function)
        this.props.onDownload(item, i, e);
}

onClick(item, i, e) {
    if (this.props.onClick instanceof Function)
        this.props.onClick(item, i, e);
}

onTitleClick(item, i, e) {
    if (this.props.onTitleClick instanceof Function)
        this.props.onTitleClick(item, i, e);
}

onForwardClick(item, i, e) {
    if (this.props.onForwardClick instanceof Function)
        this.props.onForwardClick(item, i, e);
}

loadRef(ref) {
    this.mlistRef = ref;
    if (this.props.cmpRef instanceof Function)
        this.props.cmpRef(ref);
}

onScroll(e) {
    var bottom = this.getBottom(e.target);
    this.state.scrollBottom = bottom;
    if (this.props.toBottomHeight === '100%' || bottom > this.props.toBottomHeight) {
        if (this.state.downButton !== true) {
            this.state.downButton = true;
            this.setState({
                downButton: true,
                scrollBottom: bottom,
            })
        }
    } else {
        if (this.state.downButton !== false) {
            this.state.downButton = false;
            this.setState({
                downButton: false,
                scrollBottom: bottom,
            })
        }
    }

    if (this.props.onScroll instanceof Function) {
        this.props.onScroll(e);
    }
}

toBottom(e) {
    if(!this.mlistRef)
        return;
    this.mlistRef.scrollTop = this.mlistRef.scrollHeight;
    if (this.props.onDownButtonClick instanceof Function) {
        this.props.onDownButtonClick(e);
    }
}
*/
// loadRef(ref) {
//     this.mlistRef = ref;
//     if (this.props.cmpRef instanceof Function)
//         this.props.cmpRef(ref);
// }

  render() {
    return (
        <div
            className={classNames(['rce-container-mlist'])}>
            <div
                                
                className='rce-mlist'>
                {
                    this.props.dataSource.map((x, i) => (
                        <MessageBox
                            key={i}
                            {...x}
                                         
                            
                           
                           />
                    ))
                }
            </div>
            {/* {
                this.props.downButton === true &&
                this.state.downButton &&
                this.props.toBottomHeight !== '100%' &&
                <div
                    className='rce-mlist-down-button'
                    onClick={this.toBottom.bind(this)}>
                    <FaChevronDown/>
                    {
                        this.props.downButtonBadge &&
                        <span
                            className='rce-mlist-down-button--badge'>
                            {this.props.downButtonBadge}
                        </span>
                    }
                </div>
            } */}
        </div>
    );

  }
}
