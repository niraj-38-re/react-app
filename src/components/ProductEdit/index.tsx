import * as React from 'react';


export namespace ProductEditSection {
  export interface Props {
    message: MessageItemData;
    actions: any;
    routingHistory: any;

  }

  export interface State {

  }
}



import Form from "react-jsonschema-form";
const schema = {
  title: "Product",
  type: "object",
  required: ["name"],
  properties: {
    name: { type: "string", title: "name" },
    price: { type: "string", title: "Price" },
    qty: { type: "string", title: "Quanity" }

  }
};



export class ProductEditSection extends React.Component<ProductEditSection.Props, ProductEditSection.State> {

  constructor(props?: ProductEditSection.Props, context?: any) {
    super(props, context);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }


  handleSubmit(e) {
      console.log(e);
      if(e.formData._id){
        // edit
        this.props.actions.sendEditMessage(e.formData); 
      }else{
        // create new one
        this.props.actions.sendMessage(e.formData);
        
      }
      

  }

  handleCancel(e) {
    // const text = document.getElementById('sendMsg')
    this.props.routingHistory.push('/');

  }


  render() {
    
    return (
      <div>
        <Form schema={schema}
          formData={this.props.message}
          
          onSubmit={this.handleSubmit}
          onError={null}>

          <div>
            <button onSubmit={this.handleSubmit} type="submit">Submit</button>
            <button onClick={this.handleCancel} type="button">Cancel</button>
          </div>
        </Form>





      </div>
    );
  }
}
