import { combineReducers, Reducer } from 'redux';
import { combineEpics } from 'redux-observable';

import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux'


import  { message , currentmessage } from './messages';
import { getMessagesEpic , getMessageEpic,  SendEditMessageEpic,SendMessageEpic ,  ListenMessageEpic} from '../actions/message';
 
export interface RootState {
  message: MessageStoreState;
  currentmessage: MessageItemData;    
  router: any;
  async: any;
}

export const rootReducer = combineReducers<RootState>({
  message,
  currentmessage,
  router: routerReducer
  
});


export const rootEpic = combineEpics(
  getMessagesEpic,
  getMessageEpic,
  SendMessageEpic,
  SendEditMessageEpic,
  ListenMessageEpic
);
// rebase comment added

export function registerReducer(store, name, reducer) {
  store.async[name] = reducer;
  store.replaceReducer(createReducer(store.async));
};

function createReducer (reducers) {
  return combineReducers({
    root: (state=null) => state,
    ...reducers
  });
}