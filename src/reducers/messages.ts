import { handleActions } from 'redux-actions';
import { expand } from 'rxjs/operator/expand';

import * as Actions from '../constants/actions';

// const initialState: MessageStoreState = [{
//   id:1,
//   position: 'left',
//   type: 'text',
//   title: 'Remote User A | Bot',
//   text: 'Hello ',
//   date: new Date()
// },
// {
//   id:2,
//   position: 'right',
//   type: 'text',
//   title: 'Remote User B | me',
//   text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
//   date: new Date()
// },
// ];

const initialState: MessageStoreState = [
];

export const message = handleActions<MessageStoreState, MessageItemData>({

  [Actions.ADD_MESSAGE]: (state, action) => {
    // console.log('pure reducer , action', action)
    return [...state, {

      ...action.payload,
    }];
  },


  [Actions.ADD_MESSAGES]: (state, action) => {
    // console.log(action.payload);
    return [].concat(...state, action.payload );
  },



  [Actions.EDIT_MESSAGE]: (state, action) => {
    return state.map(item => {
      return item._id === action.payload._id
        ? { ...item, ...action.payload }
        : item;
    });
  },





}, initialState);

export const currentmessage = handleActions<any, MessageItemData>({


  [Actions.SET_EDIT_MESSAGE]: (state, action) => {
    console.log('...set current edit form');
    return { ...action.payload }
  },


}, null);

