import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
// import { Router, Route, Switch } from 'react-router';
// import { createBrowserHistory } from 'history';
import { BrowserRouter, HashRouter, Router, Route, Switch, Link } from 'react-router-dom';
import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux'


import { configureStore, history } from './store';
import { App } from './containers/App';
import { ProductEdit } from './containers/Edit';

// import { AsyncComponent } from './AsyncComponent';
// import { module } from './module';


const store = configureStore();
// const history = createBrowserHistory();

// const register = module(store);
// const projects = () => register('projects', import(/* webpackChunkName: "projects" */ './modules/projects/index'));


ReactDOM.render(
  <Provider store={store}>

    <ConnectedRouter history={history}>
      <Switch>
        <Route exact={true} path="/" component={App} />
        <Route exact={true} path="/edit/:id" component={ProductEdit} />
        <Route exact={true} path="/create" component={ProductEdit} />


        {/* <Route path="/p" exact={true} component={() => <AsyncComponent moduleProvider={projects} />} /> */}
      </Switch>
    </ConnectedRouter>

  </Provider>,
  document.getElementById('root')
);
