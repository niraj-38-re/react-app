import { createAction } from 'redux-actions';
import * as Actions from '../constants/actions';




import { Observable } from 'rxjs';
import { ajax } from 'rxjs/observable/dom/ajax';
import { map, mergeMap } from 'rxjs/operators';

import * as Rx from 'rxjs';


export const addMessage = createAction<MessageItemData>(Actions.ADD_MESSAGE);
export const addMessages = createAction<[any]>(Actions.ADD_MESSAGES);

export const getMessages = createAction<MessageItemData>(Actions.GET_MESSAGES);
export const getMessage = createAction<MessageItemData>(Actions.GET_MESSAGE);

export const setEditMessage = createAction<MessageItemData>(Actions.SET_EDIT_MESSAGE);

export const sendEditMessage = createAction<MessageItemData>('SEND_EDIT_MESSAGE');

export const editMessage = createAction<MessageItemData>(Actions.EDIT_MESSAGE);

export const deleteMessage = createAction<MessageItemId>(Actions.DELETE_MESSAGE);

export const sendMessage = createAction<MessageItemData>(Actions.SEND_MESSAGE);

export const listenMessage = createAction(Actions.LISTEN_MESSAGE);

export const stoplistenMessage = createAction(Actions.STOP_LISTEN_MESSAGE);



//  example of https://redux-observable.js.org/docs/basics/Epics.html 
export const getMessagesEpic = (action$, store) => {

  return action$.ofType(Actions.GET_MESSAGES)
    .mergeMap(action => {
      return ajax.getJSON(`http://127.0.0.1:9000/api/requestlogs`)
        .flatMap((response : [MessageItemData]) => {
          console.log(response);
          let temparray =  response;
          // can use response data here
          return Observable.concat(

            Observable.of(addMessages(temparray)),
            // Observable.of(addMessage({
            //   id: 2,
            //   name: 'Product B',
            //   image:'',
            //   price: '12',
            //   qty: '123',              
            //   date: new Date()
            // }))
          )




        })
        .catch(err => {
          console.log(err);
          throw (err)
        })
    }
    );

}

// for edit view
export const getMessageEpic = (action$, store) => {

  return action$.ofType(Actions.GET_MESSAGE)
    .mergeMap(action => {
      return ajax.getJSON(`http://127.0.0.1:9000/api/requestlogs/`+action.payload._id)
        .flatMap(response => {


          return Observable.concat(

            
            Observable.of(setEditMessage(response))
          )



        })
        .catch(err => {
          console.log(err);
          throw (err)
        })
    }
    );

}

export const SendMessageEpic = (action$, store) => {


  return action$.ofType(Actions.SEND_MESSAGE)
    .mergeMap(action => {

      return ajax.post(`http://127.0.0.1:9000/api/requestlogs`,action.payload,{'Content-Type': 'application/json',})
        .map(response => {

          // console.log(response.response);
          return addMessage(response.response);


        })
        .catch(err => {
          console.log(err);
          throw (err)
        })

    }
    );
}


export const SendEditMessageEpic = (action$, store) => {
  
  
    return action$.ofType('SEND_EDIT_MESSAGE')
      .mergeMap(action => {
  
        return ajax.put(`http://127.0.0.1:9000/api/requestlogs/`+action.payload._id,action.payload,{'Content-Type': 'application/json',})
          .map(response => {
  
            // console.log(response.response);
            return editMessage(response.response);
  
  
          })
          .catch(err => {
            console.log(err);
            throw (err)
          })
  
      }
      );
  }
  

// Lazy, doesn't connect when no one is subscribed


export const ListenMessageEpic = (action$, store) => {
  return action$.ofType(Actions.LISTEN_MESSAGE)
    .mergeMap((action) => {
      console.log('in epic mergeMap ....', action);

      var wsUri = "ws://echo.websocket.org/";
      window['websocket'] = new WebSocket(wsUri);
      window['websocket'].onopen = (con) => {
        console.log("WebService Connected to ", con);
      }
      // window['websocket'].onmessage = function(evt) { console.log(evt)};


      return Observable.create(observer => {
        window['websocket'].onmessage = (evt) => {
          console.log(evt)
          observer.next(evt);
        };
      })
        .takeUntil(action$.ofType(Actions.STOP_LISTEN_MESSAGE)
          .filter(closeAction => closeAction.ticker === action.ticker)
        )
        .map(response => {
          console.log(response);
          // return ({ type: 'RECEIVED_MESSAGE', paylod: response })
          return addMessage({
            _id: Math.floor(Math.random() * 20),
            name: 'Product A',
            image:'',
            price: '12',
            qty: '123',              
            date: new Date()
          });

        })



    });
}

