import * as React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';

import { routeActions, ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux'

import * as style from './style.css';

import * as MessageActions from '../../actions/message';

import { RootState } from '../../reducers';

import { ProductEditSection } from '../../components/ProductEdit';


export namespace ProductEdit {
  export interface Props extends RouteComponentProps<void> {
    message: MessageItemData;
    actions: typeof MessageActions;

  }

  export interface State {
    /* empty */
  }
}

@connect(mapStateToProps, mapDispatchToProps)
export class ProductEdit extends React.Component<ProductEdit.Props, ProductEdit.State> {

  componentWillReceiveProps() {

  }

  

  render() {
    const { message, actions, children, history } = this.props;

    return (
      <div>

        <ProductEditSection routingHistory={history} message={message} actions={actions}></ProductEditSection>


      </div>
    );

  }
  componentDidMount() {
    if (this.props.match.params['id']) {
      this.props.actions.getMessage({ _id: this.props.match.params['id'] });
      

    } else {
      this.props.actions.setEditMessage({});
      // this.props.history.push('/');

    }

  }
}

function mapStateToProps(state: RootState) {
  return {
    message: state.currentmessage


  };
}



function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(MessageActions as any, dispatch),

  };
}
