import * as React from 'react';
import * as style from './style.css';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';


import { routeActions, ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux'


import * as MessageActions from '../../actions/message';

import { RootState } from '../../reducers';

import { MainSection } from '../../components';
import { ProductEditSection } from '../../components/ProductEdit';


export namespace App {
  export interface Props extends RouteComponentProps<void> {
    messages: MessageItemData[];
    actions: typeof MessageActions;

  }

  export interface State {
    /* empty */
  }
}

@connect(mapStateToProps, mapDispatchToProps)
export class App extends React.Component<App.Props, App.State> {

  componentWillReceiveProps() {
    // console.log('will recive props');
    // console.log(this.props);



  }





  render() {
    const { messages, actions, children, history } = this.props;

    

    return (
      <div>

        <Link to="/create">create </Link>
        <MainSection messages={messages} actions={actions} />

      </div>
    );

  }
  componentDidMount() {
    // console.log(this.props.messages)
    if (this.props.messages.length > 0) {

    } else {

      this.props.actions.getMessages({});
      this.props.actions.listenMessage();

    }


  }
}

function mapStateToProps(state: RootState) {
  return {
    messages: state.message,


  };
}



function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(MessageActions as any, dispatch),

  };
}
