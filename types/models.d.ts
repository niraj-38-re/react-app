

declare type MessageItemId = number;


declare type MessageStoreState = MessageItemData[];

declare type RefData = Object;

declare interface MessageItemData {
  _id?: MessageItemId;
  name?: string;
  image?: string;
  price?: string,
  qty?: string,  
  date?: any;
  
  
}
